package unique

func isUnique(s string) bool {
	for i, character := range s {
		for j, letter := range s {
			if i == j {
				continue
			}
			if character == letter {
				return false
			}
		}
	}
	return true
}

func isUniqueMap(s string) bool {
	var characters = make(map[rune]int)
	for index, character := range s {
		if _, ok := characters[character]; ok {
			return false
		}
		characters[character] = index
	}
	return true
}
