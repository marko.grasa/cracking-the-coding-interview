package unique

import (
	"testing"
)

var valid = []string{"", "b", "ab", "acd", "asdfjkl", "aA"}
var invalid = []string{"aa", "avva", "aaa", "asdfweryhfda", "asdfjkal"}

func helper(t *testing.T, f func(string) bool, stringArray []string, expected bool) {
	t.Helper()
	for _, input := range stringArray {
		if f(input) != expected {
			t.Errorf("Expected '%v' for string: %v", !expected, input)
		}
	}
}

func Test_isUnique(t *testing.T) {
	for _, f := range []func(string) bool{isUnique, isUniqueMap} {
		helper(t, f, valid, true)
		helper(t, f, invalid, false)
	}
}
func TestIsUnique(t *testing.T) {
	cases := []struct {
		name     string
		f        func(string) bool
		input    []string
		expected bool
	}{
		{
			"O(n^2) happy path tests",
			isUnique,
			valid,
			true,
		},
		{
			"O(n^2) failure tests",
			isUnique,
			invalid,
			false,
		},
		{
			"O(n) happy path tests",
			isUniqueMap,
			valid,
			true,
		},
		{
			"O(n) failure tests",
			isUniqueMap,
			invalid,
			false,
		},
	}

	for _, tc := range cases {
		t.Run(tc.name, func(t *testing.T) {
			helper(t, tc.f, tc.input, tc.expected)
		})
	}
}
